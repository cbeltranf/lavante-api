<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    protected $table = "stocks";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['cant'];

	public $timestamps = false;


}
