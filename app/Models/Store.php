<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{

    use SoftDeletes;
    protected $table = "stores";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];


    public function Redemptions()
    {
        return $this->hasMany('App\Models\Redemption', 'stores_id', 'id');

    }
}
