<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipio extends Model
{
    use SoftDeletes;
    protected $table = "municipios";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name','state', 'departamento_id'];



    public function Customers()
    {
        return $this->hasMany('App\Models\Customer', 'cities_id', 'id');

    }

    public function Departamento()
    {
        return $this->belongsTo('App\Models\Departamento', 'departamento_id', 'id');

    }

}
