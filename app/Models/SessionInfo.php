<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SessionInfo extends Model
{
    use SoftDeletes;
    protected $table = "session_infos";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at', 'start', 'end'];
    protected $fillable = ['users_id', 'start', 'end', 'token', "role"];// 1- rece - 2 oper
    protected $hidden = ['token'];


    public function User()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');

    }

}
