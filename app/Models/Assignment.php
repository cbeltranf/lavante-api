<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use SoftDeletes;
    protected $table = "assignments";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['is_completed', 'customers_id', 'users_id'];


    public function Customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customers_id', 'id');

    }

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');

    }

    public function Steps()
    {
        return $this->hasMany('App\Models\Step', 'assignments_id', 'id');

    }


    public function Redemptions()
    {
        return $this->hasMany('App\Models\Redemption', 'assignments_id', 'id');

    }
}
