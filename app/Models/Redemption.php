<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Redemption extends Model
{
    use SoftDeletes;
    protected $table = "redemptions";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at', 'completed', 'abandoned', 'date_limit'];
    protected $fillable = ['invoice',  'stores_id', 'users_id','assignments_id', 'terms', 'completed', 'abandoned', 'observation', 'quantity', 'amount', 'pin','cds','cds_direction', 'servientrega_id', 'date_limit'];



    public function Assignment()
    {
        return $this->belongsTo('App\Models\Assignment', 'assignments_id', 'id');
    }

    public function Store()
    {
        return $this->belongsTo('App\Models\Store', 'stores_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');
    }

    public function InvoiceSupports()
    {
        return $this->hasMany('App\Models\InvoiceSupport', 'redemptions_id', 'id');

    }

    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".invoice", 'like', "%$param%");
        $query->orWhere($this->table. ".completed", 'like', "%$param%");
        $query->orWhere($this->table. ".abandoned", 'like', "%$param%");
        $query->orWhere($this->table. ".observation", 'like', "%$param%");
    }

}
