<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Step extends Model
{
    use SoftDeletes;
    protected $table = "steps";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at', 'date'];
    protected $fillable = ['step', 'date', 'assignments_id'];


    public function Assignment()
    {
        return $this->belongsTo('App\Models\Assignment', 'assignments_id', 'id');

    }
}
