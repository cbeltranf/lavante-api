<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceSupport extends Model
{
    use SoftDeletes;
    protected $table = "invoice_supports";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['picture', 'redemptions_id'];


    public function Redemption()
    {
        return $this->hasMany('App\Models\Redemption', 'redemptions_id', 'id');

    }
}
