<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    protected $table = "customers";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'lastname', 'document', 'mobile', 'cities_id','adress', 'users_id'];



    public function Assignments()
    {
        return $this->hasMany('App\Models\Assignment', 'customers_id', 'id');

    }


    public function User()
    {
        return $this->belongsTo('App\Models\User', 'users_id', 'id');

    }


    public function City()
    {
        return $this->belongsTo('App\Models\Municipio', 'cities_id', 'id');

    }



    /*  CUSTOM SCOPES */


    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".name", 'like', "%$param%");
        $query->orWhere($this->table. ".lastName", 'like', "%$param%");
        $query->orWhere($this->table. ".mobile", 'like', "%$param%");
        $query->orWhere($this->table. ".document", 'like', "%$param%");
        $query->orWhere($this->table. ".adress", 'like', "%$param%");
    }

    public function scopeclaimedProducts($query)
    {
        $query->whereNotNull('redemptions.completed')
              ->whereNull('redemptions.abandoned')
              ->join('assignments', 'assignments.customers_id', '=', 'customers.id')
              ->join('redemptions', 'redemptions.assignments_id', '=', 'assignments.id');
    }


}
