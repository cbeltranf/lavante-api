<?php

namespace App\Http\Controllers;

use App\Models\InvoiceSupport;
use Illuminate\Http\Request;

class InvoiceSupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InvoiceSupport  $invoiceSupport
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceSupport $invoiceSupport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InvoiceSupport  $invoiceSupport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoiceSupport $invoiceSupport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InvoiceSupport  $invoiceSupport
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoiceSupport $invoiceSupport)
    {
        //
    }
}
