<?php

namespace App\Http\Controllers;

use App\Models\SessionInfo;
use App\Models\Assignment;
use App\Models\Views\Vw_AssignmentsUsers;
use Illuminate\Http\Request;

class SessionInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = SessionInfo::with('User')->whereNull('end')->get();
        return $list;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($role)
    {
        $users_id = auth()->user();
        $users_id = $users_id->id;

        $id = SessionInfo::where(['users_id' => $users_id])->whereNull('end')->get();

        $upd = SessionInfo::where(['id' =>$id[0]->id])->update(["role" => $role]);

 if($id[0]->role == 2 && $role == 1){

            $operator = Vw_AssignmentsUsers::first();
            if( $operator->id > 0 ){

                    $cnt = Assignment::where('users_id' , $users_id)->where("is_completed", '0')->orWhereNull("is_completed")->count();

                    if($cnt>0){
                        $assi=Assignment::where('users_id' , $users_id)->where("is_completed", '0')->orWhereNull("is_completed")->get();
                        $oo=Assignment::find($assi[0]->id);
                        $oo->users_id = $operator->id;
                        $oo->save();
                    }


            }
        }
        return response()->json(
            [
                'status' => [
                    'status' => 200,
                    'message' => 'Reasignado. Rol '.$role
                ]
            ],
            200
        );

     }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SessionInfo  $sessionInfo
     * @return \Illuminate\Http\Response
     */
    public function show(SessionInfo $sessionInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SessionInfo  $sessionInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SessionInfo $sessionInfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SessionInfo  $sessionInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(SessionInfo $sessionInfo)
    {
        //
    }


}
