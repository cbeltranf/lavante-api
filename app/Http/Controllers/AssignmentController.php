<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Customer;
use App\Models\Step;
use App\Models\Redemption;
use App\Models\Views\Vw_AssignmentsUsers;
use App\Models\Views\Vw_CountProductsCustomer;



use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use Propaganistas\LaravelPhone\PhoneNumber;
use Carbon\Carbon;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pageSize = $request->input("pageSize");
        $complete = $request->input("completed");
        $user = $request->input("user");

        $item = Assignment::orderBy("id","desc");
     //   if (!empty($complete)) {
            $item->where('is_completed',0);
     //   }

        if (!empty($user)) {
            $item->where('users_id', $user);
        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }
        $item->with('User');
        $item->with('Customer');
        $item->with('Steps');

        return new GlobalCollection($item->paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->validate([
            "mobile" => "required|min:10|phone:CO"
        ]);
        $data["mobile"] = PhoneNumber::make($data["mobile"])->ofCountry('CO');
        $exists = Customer::where($data)->count();

        if($exists > 0){
             $customer_existent = Customer::where($data)->get();
             $claimedProducts=Customer::where("customers.mobile", $data["mobile"])
                                        ->claimedProducts()->count();
             $total_products = Vw_CountProductsCustomer::where("customers_mobile", $data["mobile"])->get();

             $customerid = $customer_existent[0]["id"];

             if(isset($total_products[0]["product_total"]) && !is_null($total_products[0]["product_total"]) && $total_products[0]["product_total"] >9){
                return response()->json(
                    [
                        'errors' => [
                            'status' => 200,
                            'message' => 'El cliente ya obtuvo 10 unidades del producto'
                        ]
                    ],
                    200
                );
            }
        }else{
            $data["users_id"] = auth()->user()->id;
            $customer = Customer::create($data);
            $customerid = $customer->id;
        }

        $isAsigned = Assignment::where(['customers_id'=>$customerid,
                            'is_completed'=> 0 ])->count();

        if($isAsigned > 0){
            return response()->json(
                [
                    'errors' => [
                        'status' => 200,
                        'message' => 'El cliente ya se encuentra asignado'
                    ]
                ],
                200
            );
        }


        $operator = Vw_AssignmentsUsers::first();
        if($operator){

            $cant = Assignment::where("customers_id", $customerid)->where("is_completed", 0)->count();
            if($cant < 1){
                $assign = Assignment::create(['users_id'=>$operator->id, 'customers_id'=>$customerid]);
            }else{
                $assign = Assignment::where("customers_id", $customerid)->where("is_completed", 0)->get();
                $assign= $assign[0];
            }

            $step = Step::firstOrNew( array( "step" => 1, 'assignments_id' => $assign->id ) );
            $step->date = Carbon::now();
            $step->save();



            $r =  new Redemption();
            $r->assignments_id = $assign->id;
            $r->users_id = auth()->user()->id;
            $r->save();

            $response["assing"]=$assign;
            $response["redemption_id"]=$r->id;
            $response["operator"]=$assign->User;
            $response["participantPhoneFormatted"]=$assign->Customer->mobile;
            $response["customer"]=$assign->Customer->mobile;


            return response()->json($response);

        }else{
                return response()->json(
                        [
                            'errors' => [
                                'status' => 200,
                                'message' => 'No hay operadores disponibles'
                            ]
                        ],
                        200
                    );

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function show(Assignment $assignment)
    {
        $assignment->Steps;
        $assignment->Customer;
        $assignment->User;

        return response()->json($assignment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assignment $assignment)
    {
        $data = $request->validate([
            "is_completed" => "nullable|integer|max:1",
            "users_id" => "nullable|exists:users,id",
            ]);

            //  if(empty($data['is_completed']))
                unset($data['is_completed']);

            if(empty($data['users_id']))
                    unset($data['users_id']);

            if(!empty($data['users_id'])){
                $operator = Vw_AssignmentsUsers::where('id', $data['users_id'])->first();
                if($operator){
                }else{
                        return response()->json(
                                [
                                    'errors' => [
                                        'status' => 200,
                                        'message' => 'El operador seleccionado no se encuentra disponible'
                                    ]
                                ],
                                200
                            );

                    }
            }

            $assignment->update($data);
            $assignment->Customer;
            $assignment->Steps;

            /*if($data['is_completed']==1){
                Redemption::create(['assignments_id' => $assignment->id,
                'abandoned'=>Carbon::now(),
                'Users_id'=> $data["users_id"] = auth()->user()->id]);
            }
*/
            $response["assing"]=$assignment;
            $response["operator"]=$assignment->User;
            $response["participantPhoneFormatted"]=$assignment->Customer->mobile;
            $response["customer"]=$assignment->Customer->mobile;

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assignment $assignment)
    {
        $assignment->is_completed = 1;
        $assignment->save();
        $assignment->Customer;

        $cant = Redemption::where(["assignments_id" => $assignment->id])->count();
        if($cant > 0){
            Redemption::where(["assignments_id" => $assignment->id])->update(["abandoned" => Carbon::now()]);
        }

        //$item = $assignment->delete();

        $response["messages"] = "Se ha anulado el proceso con éxito.";
        $response["status"] = 200;

        return response()->json($response);

    }
}
