<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Redemption;
use App\Models\Assignment;
use App\Models\Step;
use App\Models\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Resources\GlobalCollection;
use Propaganistas\LaravelPhone\PhoneNumber;
Use GuzzleHttp\Client as Guzzle;
use DB;



class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "name";
        }

        $item = Customer::orderBy($sortField, $sortOrder);

        if (empty($filter) || $filter == "*") {

            $item->filterValue($filterValue);

        } else {

            $item->where($filter, 'like', "%$filterValue%");

        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        return new GlobalCollection($item->paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "name" => "nullable|max:255",
            "lastname" => "nullable|max:255",
            "adress" => "nullable|max:255",
            "document" => "nullable|max:100|unique:customers",
            "mobile" => "required|max:100|min:10|phone:CO",
            "cities_id" => "nullable|integer"
        ]);
        $data["users_id"] = auth()->user()->id;

        $data["mobile"] = trim(PhoneNumber::make($data["mobile"])->ofCountry('CO'));
        $first_char= substr($data["mobile"], 0, 1);
        if($first_char != "+"){
            $data["mobile"] = "+".$data["mobile"];
            }
        $data = request()->only('name', 'lastname', 'mobile','adress', 'cities_id', 'document');
        $data = array_filter($data);
        $inserted = Customer::firstOrNew($data);

        return response()->json($inserted);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        $customer->City;
        return response()->json($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $data = $request->validate([
            "name" => "nullable|max:255",
            "lastname" => "nullable|max:255",
            "document" => "nullable|max:100",
            "mobile" => "required|max:100|min:10|phone:CO",
            "cities_id" => "nullable|integer",
            "adress" => "nullable|max:255",
            "redemptons_id" => "nullable|integer",
            "assigments_id" => "nullable|integer",
            "comments" => "nullable|string"
        ]);


        $data["document"]=preg_replace("/[^0-9]+/", "", strtoupper($data["document"]));

        $vald = DB::table('customers as ct')
        ->where('ct.document', '=', $data["document"])
        ->count();

        if($vald > 0){

        $redemptions =  $data["redemptons_id"];
        $assigment =  $data["assigments_id"];

        $vald = DB::table('customers as ct')
        ->select("ct.id as id")
        ->where('ct.document', '=',  $data["document"])
        ->get();

        Assignment::where("id", '=', $assigment)->update(["customers_id" => $vald[0]->id]);
        //Redemption::where("id", '=', $redemptions)->update(["customers_id" => $vald[0]->id]);

        $customer =  Customer::find($vald[0]->id);

        }


        if(!empty($data["redemptons_id"]) && !empty($data["assigments_id"])){
        $redemptions =  $data["redemptons_id"];
        $assigment =  $data["assigments_id"];
        $coments =  $data["comments"];
        $adress =  $data["adress"];
        $city =  $data["cities_id"];

        $r = Redemption::find($redemptions);

        $munDep = DB::table('municipios as ct')
        ->join('departamentos as dp', 'dp.id', '=', 'ct.departamento_id')
        ->select('ct.name as municipio', 'dp.name as departamento')
        ->where('ct.id', $city)
        ->get();

        $direccion = urlencode($adress.', '.$munDep[0]->municipio.', '.$munDep[0]->departamento.', Colombia');
        $url = "https://maps.google.com/maps/api/geocode/json?address=".$direccion."&key=AIzaSyB8ZwgpE1YjoabdGss6ZZ1EeZPIPmSryzU";
        $client = new Guzzle();
        $response = $client->request('GET', $url);
        $response = json_decode($response->getBody());

        $result = DB::table('servientrega as s')
        ->join('cities as ct', 'ct.id', '=', 's.cities_id')
        ->select('s.id as id', 's.nombre as nombre', 's.direccion as direccion', 'ct.name as city', DB::raw("concat(s.lat, ' ', s.lon) as coordenadas "),
        DB::raw("(
                   GLENGTH(
                           LINESTRINGFROMWKB(
                                              LINESTRING(
                                                          `point`,
                                                               GEOMFROMTEXT('POINT(".$response->results[0]->geometry->location->lat." ".$response->results[0]->geometry->location->lng.")')
                                                         )
                                            )
                            )
                    )
                      AS distance")

         )->orderBy('distance', 'asc')
         ->limit(1)->get();


//        $output["cdc"]= $result;
  //      $output["coords"]=$response->results[0]->geometry->location->lat." ".$response->results[0]->geometry->location->lng;
//        $output["maps_object"]=$response;
//        $output["direccion"]=$direccion;

        $url = "https://admin.dataella.com/Plataformas/Desarrollos/Servientrega/DT_SVRLT/V1/mensaje/msjmt";

        $Serv = new Guzzle();

        $to_send = array();
        $to_send["cedula"]=$data["document"];
        $to_send["nombre"]= $this->eliminar_tildes($data["name"].' '.$data["lastname"]);
        $to_send["correo"]="noaplica@noaplica.com";
        $to_send["celular"]=$data["mobile"];
        $to_send["planchas"]=$r->quantity;
        $to_send["valor"]=$r->quantity * 50000;
        $to_send["ciudad_sucursal"]=$result[0]->city;
        $to_send["nombre_sucursal"]=$this->eliminar_tildes($result[0]->nombre);
        $to_send["direccion_sucursal"]=$this->eliminar_tildes($result[0]->direccion);

        //dd($to_send);
        //$to_send = json_encode($data);

        $Serv_r = $Serv->request('POST', $url, [
                                    'headers' => [
                                        'Content-Type' => 'application/json',
                                        'Accept'     => 'application/json',
                                        'Authorization' => 'Basic R05TcnZMVlQkMTgvd3M6U2UqJGRpMjUqcnQvVw=='
                                    ],
                                    'json' => $to_send
                                  ], $to_send);
        $Serv_r = json_decode($Serv_r->getBody());




        $return["servientrega"]["Guia"] = $Serv_r->messages[0]->PIN_EFECTY;
        $return["servientrega"]["Sede"] = $Serv_r->messages[0]->CDS;
        $return["servientrega"]["Precio"] = $r->quantity * 50000;
        $return["servientrega"]["Direccion"] = $Serv_r->messages[0]->DIRECCION;
        $return["servientrega"]["Descripcion"] = $Serv_r->messages[0]->description;
        $return["servientrega"]["Fecha_Limite"] =  $random = Carbon::parse($Serv_r->messages[0]->FECHA_VIGENCIA);
        $return["servientrega"]["Fecha_Inicio"] =  $random = Carbon::parse($Serv_r->messages[0]->FECHA_INICIAL);





        $r->observation = $r->observation . $coments . " | Numero de Guia: ". $return["servientrega"]["Guia"] . " +  Fecha Limite entrega: ". $return["servientrega"]["Fecha_Limite"]." | ".json_encode($return["servientrega"]);
        $r->pin = $Serv_r->messages[0]->PIN_EFECTY;
        $r->cds = $Serv_r->messages[0]->CDS;
        $r->cds_direction = $Serv_r->messages[0]->DIRECCION;
        $r->servientrega_id = $result[0]->id;
        $r->date_limit = $Serv_r->messages[0]->FECHA_VIGENCIA;
        $r->completed = Carbon::now();
        $r->save();

        //$stockUpdt = Stock::first();
        //$stockUpdt->cant = $stockUpdt->cant - $r->quantity;
        //$stockUpdt->save();


        Assignment::where("id", $assigment)->update(["is_completed" => '1']);
        $step = Step::firstOrNew( array( "step" => 6, 'assignments_id' => $assigment ) );
        $step->date = Carbon::now();
        $step->save();



                 }
        unset($data["redemptons_id"]);
        unset($data["assigments_id"]);
        unset($data["comments"]);

        $data["users_id"] = auth()->user()->id;
        $data["mobile"] = PhoneNumber::make($data["mobile"])->ofCountry('CO');


        $data = request()->only('name', 'lastname', 'mobile', 'adress', 'cities_id', 'document');
        $data = array_filter($data);


        $customer->update($data);
        $customer->City;
        $return["customer"]= $customer;
        return response()->json($return);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $item = $customer->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }


    public function eliminar_tildes($cadena){

        //Codificamos la cadena en formato utf8 en caso de que nos de errores
        $cadena = utf8_encode($cadena);

        //Ahora reemplazamos las letras
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena );

        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );

        return $cadena;
    }


    public function procesar()
    {

    $items[]=array("cel"=>3188312794, "nombre" => "YISETH RUIDIAZ RODRIGUEZ", "documento" => "45562587", "direccion" => "CRA 17 #62-85", "ciudad" => "CARTAGENA");
    $items[]=array("cel"=>3117214787, "nombre" => "LUZ NELLY GIRALDO SIERRA", "documento" => "25019209", "direccion" => "CRA 10# 14-34", "ciudad" => "QUIMBAYA, QUINDIO");
    $items[]=array("cel"=>3207490267, "nombre" => "Diana Esperanza Idarraga Arbelaez", "documento" => "31386691", "direccion" => "Diagonal 25A # 7-119", "ciudad" => "DOSQUEBRADAS, RISARALDA");
    $items[]=array("cel"=>3106915315, "nombre" => "DANIA VANESSA RAMOS ANGULA", "documento" => "1082690943", "direccion" => "CRA 39 #5A 104 ", "ciudad" => "TEQUENDAMA, CALI");
    $items[]=array("cel"=>3205095415, "nombre" => "Yesica Katherine Muñoz ausecha", "documento" => "1061600933", "direccion" => "CRA 9 #22b-18", "ciudad" => "POPAYAN");
    $items[]=array("cel"=>3162710339, "nombre" => "MARLLERLY CATALINA RUEDA ALVAREZ", "documento" => "32298280", "direccion" => "TRANSVERSAL 33 DIAGONAL 3026", "ciudad" => "ENVIGADO");
    $items[]=array("cel"=>3015442205, "nombre" => "VICTOR MANUEL REDONDO PEñA", "documento" => "7451595", "direccion" => "CRA 69 # 81-77", "ciudad" => "BARRANQUILLA");
    $items[]=array("cel"=>3122755710, "nombre" => "LUZ YANETH VALENCIA CARDONA", "documento" => "30353243", "direccion" => "CLL 70B #23B-161", "ciudad" => "MANIZALES");
    $items[]=array("cel"=>3023566868, "nombre" => "MERCEDES CABALLERO CACERES", "documento" => "63443901", "direccion" => "CLL 35 #27-70", "ciudad" => "BUCARAMANGA");
    $items[]=array("cel"=>3102242920, "nombre" => "AIDA RODRIGUEZ LAGUNA", "documento" => "39533941", "direccion" => "CRA 103A #132-51", "ciudad" => "BOGOTA");
    $items[]=array("cel"=>3197376901, "nombre" => "ANDREINA MARIA MARTINEZ LUZARDO", "documento" => "1127618766", "direccion" => "CLL 17A #18- 68", "ciudad" => "FUSAGASUGA");
    $items[]=array("cel"=>3137252497, "nombre" => "VIVIANA ANDREA RAMIREZ BEDOYA", "documento" => "43983937", "direccion" => "CLL 57A #68 -08", "ciudad" => "BELLO, ANTIOQUIA");
    $items[]=array("cel"=>3002878147, "nombre" => "DIANA XIMENA RIVERO DIAZ", "documento" => "52928266", "direccion" => "CLL 70B #23B-161", "ciudad" => "MANIZALES");
$i = 0;
foreach($items as $a){
        $data["document"]=preg_replace("/[^0-9]+/", "", strtoupper($a["documento"]));


        $direccion = urlencode($a["direccion"].', '.$a["ciudad"].', Colombia');
        $url = "https://maps.google.com/maps/api/geocode/json?address=".$direccion."&key=AIzaSyB8ZwgpE1YjoabdGss6ZZ1EeZPIPmSryzU";
        $client = new Guzzle();
        $response = $client->request('GET', $url);
        $response = json_decode($response->getBody());

        $result = DB::table('servientrega as s')
        ->join('cities as ct', 'ct.id', '=', 's.cities_id')
        ->select('s.id as id', 's.nombre as nombre', 's.direccion as direccion', 'ct.name as city', DB::raw("concat(s.lat, ' ', s.lon) as coordenadas "),
        DB::raw("(
                   GLENGTH(
                           LINESTRINGFROMWKB(
                                              LINESTRING(
                                                          `point`,
                                                               GEOMFROMTEXT('POINT(".$response->results[0]->geometry->location->lat." ".$response->results[0]->geometry->location->lng.")')
                                                         )
                                            )
                            )
                    )
                      AS distance")

         )->orderBy('distance', 'asc')
         ->limit(1)->get();

        $url = "https://admin.dataella.com/Plataformas/Desarrollos/Servientrega/DT_SVRLT/V1/mensaje/msjmt";

        //$Serv = new Guzzle();

        $to_send = array();
        $to_send["cedula"]=$data["document"];
        $to_send["nombre"]= $this->eliminar_tildes($a["nombre"]);
        $to_send["correo"]="noaplica@noaplica.com";
        $to_send["celular"]=$a["cel"];
        $to_send["planchas"]=1;
        $to_send["valor"]=50000;
        $to_send["ciudad_sucursal"]=$result[0]->city;
        $to_send["nombre_sucursal"]=$this->eliminar_tildes($result[0]->nombre);
        $to_send["direccion_sucursal"]=$this->eliminar_tildes($result[0]->direccion);

        $Serv_r = $Serv->request('POST', $url, [
                                    'headers' => [
                                        'Content-Type' => 'application/json',
                                        'Accept'     => 'application/json',
                                        'Authorization' => 'Basic R05TcnZMVlQkMTgvd3M6U2UqJGRpMjUqcnQvVw=='
                                    ],
                                    'json' => $to_send
                                  ], $to_send);
        $Serv_r = json_decode($Serv_r->getBody());

        $return[$i]["servientrega"]["Guia"] = $Serv_r->messages[0]->PIN_EFECTY;
        $return[$i]["servientrega"]["Sede"] = $Serv_r->messages[0]->CDS;
        $return[$i]["servientrega"]["Precio"] = 50000;
        $return[$i]["servientrega"]["Direccion"] = $Serv_r->messages[0]->DIRECCION;
        $return[$i]["servientrega"]["Descripcion"] = $Serv_r->messages[0]->description;
        $return[$i]["servientrega"]["Fecha_Limite"] =  $random = Carbon::parse($Serv_r->messages[0]->FECHA_VIGENCIA);
        $return[$i]["servientrega"]["Fecha_Inicio"] =  $random = Carbon::parse($Serv_r->messages[0]->FECHA_INICIAL);
        $i++;
//sss
        sleep (1);
}
        return response()->json($return);
    }








}
