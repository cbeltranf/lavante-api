<?php

namespace App\Http\Controllers;

use App\Models\Redemption;
use App\Models\Assignment;
use App\Models\InvoiceSupport;
use App\Models\Step;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use App\Models\Views\Vw_CountProductsCustomer;
Use Carbon\Carbon;

class RedemptionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "invoice";
        }

        $item = Redemption::orderBy($sortField, $sortOrder);

        if (empty($filter) || $filter == "*") {

            $item->filterValue($filterValue);

        } else {

            $item->where($filter, 'like', "%$filterValue%");

        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        $item->with('Customers');
        $item->with('Users');
        $item->with('Stores');
        $item->with('InvoiceSupports');


        return new GlobalCollection($item->paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Redemption  $redemption
     * @return \Illuminate\Http\Response
     */
    public function show(Redemption $redemption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Redemption  $redemption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $redemption = Redemption::find($id);

        $data = $request->validate([
            "quantity" => "required|integer|min:1|max:10",
            "amount" => "required|numeric|min:50000|max:5000000"
        ]);

        $limit = $data["amount"] / 50000;
        $limit = round($limit, PHP_ROUND_HALF_DOWN);

        if($data["quantity"] > $limit){
            return response()->json(
                [
                    'errors' => [
                        'status' => 200,
                        'message' => 'La cantidad de producto solicitada no es cubierta por el monto de la factura.'
                    ]
                ],
                200
            );
        }


        $redemption->update($data);


       // $assign = Assignment::where("id", $redemption->assignment_id)->where("is_completed", '0')->get();
        $step = Step::firstOrNew( array( "step" => 5, 'assignments_id' => $redemption->assignments_id ) );
        $step->date = Carbon::now();
        $step->save();


         return $redemption;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Redemption  $redemption
     * @return \Illuminate\Http\Response
     */
    public function destroy(Redemption $redemption)
    {

    }


    public function uploadInvoicePhoto(Request $request, $id)
    {
        $redemption = Redemption::find($id);
        $data = $request->validate([
            "photo" => "required"
        ]);

        $new_name = time() . '.' . $request->photo->extension();
        $path = $request->photo->storeAs('images/invoices', $new_name, 'public');
        unset($data["photo"]);
        $data["picture"] = $path;

        $invoice = InvoiceSupport::create(array('redemptions_id' => $redemption->id, 'picture' => $data["picture"] ));

        #$assign = Assignment::where("id", $redemption->assignments_id)->where("is_completed", '0')->get();
        $step = Step::firstOrNew( array( "step" => 4, 'assignments_id' => $redemption->assignments_id ) );
        $step->date = Carbon::now();
        $step->save();



        return response()->json(["photo"=>asset($path)]);
    }


    public function uploadTerms(Request $request, $id)
    {
        $data = $request->validate([
            "photo" => "required"
        ]);

        $data["customers_id"] = $id;

        $assign = Assignment::where(["customers_id" => $data["customers_id"]])->where(["is_completed" => '0'])->get();
        $actual = Redemption::where(['assignments_id' => $assign[0]->id])->whereNull('completed')->whereNull('abandoned')->get();

        $new_name = time() . '.' . $request->photo->extension();
        $path = $request->photo->storeAs('images/terms', $new_name, 'public');
        unset($data["photo"]);
        $data["terms"] = $path;

        $redem = Redemption::firstOrNew(["id" => $actual[0]->id]);
        $redem->assignments_id = $assign[0]->id;
        $redem->terms = $path;
        $redem->save();

        $assign = Assignment::where(["customers_id"=> $data["customers_id"]])->where(["is_completed" => '0'])->get();
        $step = Step::firstOrNew( array( "step" => 2, 'assignments_id' => $assign[0]->id ) );
        $step->date = Carbon::now();
        $step->save();

        $updated = Redemption::where(["id" => $actual[0]->id])->get();
        $updated = $updated[0];
        $updated->terms = asset($updated->terms);
        return response()->json(["photo"=>$updated->terms]);
    }

    public function valid(Request $request)
    {
        $data = $request->validate([
            "invoice" => "required|max:255",
            "stores_id" => "required|integer|exists:stores,id",
            "customers_id" => "required|integer|exists:customers,id",
            "assignments_id" => "required|integer|exists:assignments,id",
        ]);

        $data["invoice"] = preg_replace("/[^a-zA-Z0-9]+/", "", strtoupper($data["invoice"]));

        $redem = Redemption::where('invoice', $data["invoice"])
        ->where('stores_id', $data["stores_id"])
        ->whereNotNull('completed')->whereNull('abandoned')->count();

        if($redem < 1 ){// No redemptions
        $actual = Redemption::where('assignments_id', $data["assignments_id"])->whereNull('completed')->whereNull('abandoned')->get();

        Redemption::where("id", $actual[0]->id)->update(["invoice"=> $data["invoice"], "stores_id"=> $data["stores_id"]]);

        $assign = Assignment::where('id', $data["assignments_id"])->get();
        $step = Step::firstOrNew( array( "step" => 3, 'assignments_id' => $data["assignments_id"] ) );
        $step->date = Carbon::now();
        $step->save();
        }else{
            $actual = Redemption::where('assignments_id', $data["assignments_id"])->whereNull('stores_id')->whereNull('invoice')->whereNull('completed')->whereNull('abandoned')->get();
            #$redem = Redemption::where(['id'=> $actual[0]->id])->update(["abandoned" => Carbon::Now()]);
            $assign = Assignment::where('id', $data["assignments_id"])->where("is_completed", '0')->get();
            #$assign_upd = Assignment::where('id', $data["assignments_id"])->where("is_completed", '0')->update(["is_completed" => '1']);
            #Step::create(["step"=>3, 'date'=>Carbon::now(), 'assignments_id' => $assign[0]->id]);
            #$step = Step::firstOrNew( array( "step" => 3, 'assignments_id' => $data["assignments_id"] ) );
            #$step->date = Carbon::now();
            #$step->save();
            return response()->json(
                [
                    'errors' => [
                        'status' => 200,
                        'message' => 'La factura '.$data["invoice"]. ' ya fue utilizada.'
                    ]
                ],
                200
            );

        }

        return response()->json($actual);
    }
}
