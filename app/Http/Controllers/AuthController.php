<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User as User;
use App\Models\SessionInfo as SessionInfo;
use App\Models\Assignment;
use App\Models\Views\Vw_AssignmentsUsers;
use Illuminate\Http\Request;
use JWTAuth;
use Carbon\Carbon;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = request(['document', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized. Check Credentials']);
        }

        $Auth_user = auth()->user();
        $MyUser = User::find($Auth_user->id)->toArray();
        $response = array();
        $response = $MyUser;
        #$response['pic'] = asset($MyUser["pic"]);
        $response["expires_in"] = auth()->factory()->getTTL() * 20160;
        $response["accessToken"] = $token;
        $response["refreshToken"] = $token;

        SessionInfo::whereNull('end')
          ->where('users_id', $Auth_user->id)
          ->update(['end' => Carbon::now()]);

        $session = new SessionInfo();
        $session->users_id = $Auth_user->id;
        $session->start = Carbon::now();
        $session->token = $token;
        $session->save();

        return response()->json($response);

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $Auth_user = auth()->user();
        $old_token = $request->bearerToken();
        $new_token = auth()->refresh();
        SessionInfo::where('token', $old_token)
          ->update(['token' => $new_token]);
        $MyUser = User::find($Auth_user->id)->toArray();
        $response = array();
        $response['user']=$MyUser;
        $response['token'] = $new_token;

        return response()->json($response);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $token = $request->bearerToken();

        $id = SessionInfo::where(['token' => $token])->whereNull('end')->get();
        $id_user = $id[0]->users_id;
        if($id[0]->role == 2){
            $operator = Vw_AssignmentsUsers::first();
            $ass = Assignment::where(['users_id' => $id[0]->users_id ])->where("is_completed", '0')->orWhereNull("is_completed")->update(["users_id" => $operator->id]);

        }

        User::where("id", $id_user)->update(["phone"=>"+57320".rand(1111111, 9999999)]);
        SessionInfo::whereNull('end')
          ->where('token', $token)
          ->update(['end' => Carbon::now()]);

        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        $old_token = $request->bearerToken();
        $new_token = auth()->refresh();
        $response = $this->respondWithToken($new_token);
        SessionInfo::where('token', $old_token)
          ->update(['token' => $new_token]);
        return $response;
    }

    public function invalidate()
    {
        #auth()->invalidate();
        // Pass true as the first param to force the token to be blacklisted "forever".
        auth()->invalidate(true);

        return response()->json(['message' => 'Token Invalidated successully.']);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 20160
        ]);
    }
}
