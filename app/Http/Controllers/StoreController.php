<?php

namespace App\Http\Controllers;

use App\Models\Store;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
Use GuzzleHttp\Client as Guzzle;
use DB;

class StoreController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");

        $item = Store::orderBy("name", "asc");
        $item->where("name", 'like', "%$filterValue%");

        if (empty($pageSize)) {
            $pageSize = 1000;
        }

        return new GlobalCollection($item->paginate($pageSize));    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "name" => "required|unique:stores|max:150"
        ]);

        $InsertId = Store::create($data);
        return response()->json($InsertId);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        //
    }

    public function maps(Request $request){
        $direccion = urlencode($request->input("direccion"));
        $url = "https://maps.google.com/maps/api/geocode/json?address=".$direccion."&key=AIzaSyB8ZwgpE1YjoabdGss6ZZ1EeZPIPmSryzU";
        $client = new Guzzle();
        $response = $client->request('GET', $url);
        $response = json_decode($response->getBody());

        $result = DB::table('servientrega as s')
        ->join('cities as ct', 'ct.id', '=', 's.cities_id')
        ->select('s.id as id', 's.nombre as nombre', 's.direccion as direccion', 'ct.name as city', DB::raw("concat(s.lat, ' ', s.lon) as coordenadas "),
        DB::raw("(
                   GLENGTH(
                           LINESTRINGFROMWKB(
                                              LINESTRING(
                                                          `point`,
                                                               GEOMFROMTEXT('POINT(".$response->results[0]->geometry->location->lat." ".$response->results[0]->geometry->location->lng.")')
                                                         )
                                            )
                            )
                    )
                      AS distance")

         )->orderBy('distance', 'asc')
         ->limit(1)->get();

         $output["cdc2"]= $result;
         $output["coords"]=$response->results[0]->geometry->location->lat." ".$response->results[0]->geometry->location->lng;
         $output["maps_object"]=$response;

         return response()->json($output);
    }

    public function getNearlyPoint(Request $request){

        $dirección = $request->input("dirección");

        DB::table('servientrega')
        ->where('id', $cs->id)
        ->update(['point' => DB::raw("POINTFROMTEXT('POINT(".$cs->lat." ".$cs->lon.")')") ]);

        $result = DB::table('servientrega')
        ->where('id', $cs->id)
        ->select('id', 'nombre', 'direccion', DB::raw("(
                                                        GLENGTH(
                                                            LINESTRINGFROMWKB(
                                                                LINESTRING(
                                                                `point`,
                                                                GEOMFROMTEXT('POINT(4.724228 -74.0253875)')
                                                                           )
                                                                                )
                                                                )
                                                        )
                                                        AS distance")
         )->orderBy('distance', 'asc')
         ->limit(1)->get();

    }
}
