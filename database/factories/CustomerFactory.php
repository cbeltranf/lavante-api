<?php

use Faker\Generator as Faker;
use Faker\Provider\es_ES\Person as fakePerson;
use Faker\Provider\es_ES\PhoneNumber as fakeMobile;
use Propaganistas\LaravelPhone\PhoneNumber;

$factory->define(App\Models\Customer::class, function (Faker $faker) {

     $faker->addProvider(new fakePerson($faker));
     $faker->addProvider(new fakeMobile($faker));

    return [
        'name' => $faker->name,
        'lastName' => $faker->lastName,
        'document' => $faker->dni, // secret
        'mobile' => PhoneNumber::make( $faker->phoneNumber )->ofCountry('CO'),
        'cities_id' => rand (1, 3),
        'users_id' => 1,
    ];
});
