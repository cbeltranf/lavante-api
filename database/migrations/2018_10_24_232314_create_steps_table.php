<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('step');
            $table->dateTime('date');
            $table->integer('assignments_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('steps', function($table) {
            $table->foreign('assignments_id')->references('id')->on('assignments')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('steps');
    }
}
