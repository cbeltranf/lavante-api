<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateVwAssignmentsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( "CREATE OR REPLACE  VIEW vw__assignments_users AS
        SELECT  COUNT(`a`.`id`) AS `cant`, `u`.`id` AS `id`  FROM session_infos AS si
        INNER JOIN users AS u ON si.users_id = u.id
        LEFT JOIN assignments AS a ON a.users_id = u.id AND `a`.`is_completed` != 1
        WHERE ISNULL(si.end)  AND si.role = 2
        GROUP BY `u`.`id`
        ORDER BY `cant` ASC;" );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS vw__assignments_users;' );
    }
}
