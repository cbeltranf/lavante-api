<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateVwCountProductsCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( "CREATE OR REPLACE  VIEW vw__count_products_customer AS
        SELECT SUM(r.`quantity`) AS product_total, c.id AS customers_id, c.name AS customers_name, c.`lastName` AS customers_last_name, c.`mobile` AS customers_mobile , r.id AS redemption_id, a.id AS assigment_id FROM customers AS c
INNER JOIN `assignments` AS a ON a.`customers_id` = c.`id`
INNER JOIN `redemptions` AS r ON r.`assignments_id` = a.id
WHERE r.completed IS NOT NULL AND abandoned  IS NULL
GROUP BY customers_mobile ;" );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS vw__count_products_customer;' );
    }
}
