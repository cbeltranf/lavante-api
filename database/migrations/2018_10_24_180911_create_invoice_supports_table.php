<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_supports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('picture');
            $table->integer('redemptions_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('invoice_supports', function($table) {
            $table->foreign('redemptions_id')->references('id')->on('redemptions')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_supports');
    }
}
