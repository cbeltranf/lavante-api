<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('lastName')->nullable();
            $table->string('document', 100)->unique()->nullable();
            $table->string('mobile', 100)->unique();
            $table->integer('cities_id')->unsigned()->nullable();
            $table->string('adress');
            $table->integer('users_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('customers', function($table) {

            $table->foreign('cities_id')->references('id')->on('cities')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('users_id')->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
