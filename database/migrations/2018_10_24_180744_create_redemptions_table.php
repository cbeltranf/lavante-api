<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedemptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redemptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice', 100)->nullable();
           # $table->integer('customers_id')->unsigned();
            $table->integer('stores_id')->unsigned()->nullable();
            $table->integer('users_id')->unsigned();
            $table->integer('assignments_id')->unsigned();
            $table->dateTime('completed')->nullable();
            $table->dateTime('abandoned')->nullable();
            $table->integer('quantity')->unsigned()->default('0');
            $table->float('amount', 8, 2)->unsigned()->default('0.0');
            $table->string('terms')->nullable();
            $table->longText('observation')->nullable();
            $table->string('pin')->nullable();
            $table->string('cds')->nullable();
            $table->string('cds_direction')->nullable();
            $table->integer('servientrega_id')->nullable();
            $table->dateTime('date_limit')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('redemptions', function($table) {
           # $table->foreign('customers_id')->references('id')->on('customers')
            #->onDelete('cascade')
            #->onUpdate('cascade');

             $table->foreign('assignments_id')->references('id')->on('assignments')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('stores_id')->references('id')->on('stores')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('users_id')->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redemptions');
    }
}
