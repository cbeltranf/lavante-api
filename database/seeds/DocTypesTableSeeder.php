<?php

use Illuminate\Database\Seeder;

class DocTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\DocType::insert([
            'name' => 'Cedula',
            'abb' => 'CC'
        ]);

        \App\Models\DocType::insert([
            'name' => 'Tarjeta de Identidad',
            'abb' => 'TI'
        ]);

        \App\Models\DocType::insert([
            'name' => 'Cedula de Extrangeria',
            'abb' => 'CE'
        ]);

    }
}
