<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::insert([
            'name' => 'Carlos Andres',
            'lastname' => 'Beltran Franco',
            'password' =>  bcrypt('12345'),
            'phone' => '3201111111',
            'document'=> '11111'
            #,'pic'=> 'images/profile_pic/nouser.png',
        ]);

        \App\Models\User::insert([
            'name' => 'Cristian Chayan',
            'lastname' => 'Cedeño',
            'password' =>  bcrypt('12345'),
            'phone' => '3152222222',
            'document'=> '22222'
        ]);

        \App\Models\User::insert([
            'name' => 'Jorge',
            'lastname' => 'Nitales',
            'password' =>  bcrypt('12345'),
            'phone' => '3153333333',
            'document'=> '33333'
        ]);

        \App\Models\User::insert([
            'name' => 'Godofredo',
            'lastname' => 'Rosales',
            'password' =>  bcrypt('12345'),
            'phone' => '3154444444',
            'document'=> '44444'
        ]);

    }
}

