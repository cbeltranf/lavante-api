<?php

use Illuminate\Database\Seeder;

class CustomersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Customer::class, 5)->create();
    }
}
